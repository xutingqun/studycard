package com.studycard.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.example.studycard.R;
import com.avoscloud.chat.adapter.BaseListAdapter;
import com.avoscloud.chat.adapter.AddFriendAdapter.ClickListener;
import com.avoscloud.chat.avobject.User;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.ui.view.ViewHolder;

import java.util.List;

public class CourseCommentAdapter extends BaseListAdapter<AVObject> {
	
	public CourseCommentAdapter(Context context, List<AVObject> list) {
		super(context, list);
	}
	
	@Override
	public View getView(int position, View conView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (conView == null) {
			conView = inflater.inflate(R.layout.comment_item, null);
		}
		final AVObject contact = datas.get(position);
		TextView comment_body = ViewHolder.findViewById(conView,
				R.id.comment_body);
		TextView comment_name = ViewHolder.findViewById(conView,
				R.id.comment_name);
		TextView comment_score = ViewHolder.findViewById(conView,
				R.id.comment_score);
		TextView comment_time = ViewHolder.findViewById(conView,
				R.id.comment_time);
		
		comment_body.setText(contact.getString("comment_body"));
		comment_name.setText(contact.getString("comment_name"));
		comment_time.setText(contact.getString("comment_time"));
	
		return conView;
	}

}
