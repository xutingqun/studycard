package com.studycard.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.studycard.R;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SubjectManageAdapter extends BaseAdapter {

	private Context context;

	private ArrayList<HashMap<String, Object>> list;

	public SubjectManageAdapter(Context context,
			ArrayList<HashMap<String, Object>> list) {
		this.context = context;
		this.list = list;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewSubjectHolder holder;
		if (convertView == null) {
			
			convertView = LayoutInflater.from(context).inflate(

					R.layout.subjectmanage_item, null);

			holder = new ViewSubjectHolder();

			holder.teacher_name = (TextView) convertView
					.findViewById(R.id.teacher_names);
			holder.teacher_description = (TextView) convertView
					.findViewById(R.id.teacher_descriptions);
			holder.subject_arrangement = (TextView) convertView
					.findViewById(R.id.course_arrangements);
			convertView.setTag(holder);
		} else {
			holder = (ViewSubjectHolder) convertView.getTag();

		}
		String teacher = list.get(position).get("teacher").toString();
		if (!TextUtils.isEmpty(teacher)) {

			holder.teacher_name.setText(teacher);
		}
		
		String teacher_descriptions = list.get(position)
				.get("teacher_description").toString();
		if (!TextUtils.isEmpty(teacher_descriptions)) {

			holder.teacher_description.setText(teacher_descriptions);
		}
		
		String subject_arrangements = list.get(position)
				.get("course_arrangement").toString();

		if (!TextUtils.isEmpty(subject_arrangements)) {

			holder.subject_arrangement.setText(subject_arrangements);
		}

		return convertView;

	}

	class ViewSubjectHolder {
		private TextView teacher_name;
		private TextView teacher_description;
		private TextView subject_arrangement;

	}

}