package com.studycard.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.studycard.bean.CategoryBean;

import android.R.integer;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class MyExpandanleListViewAdapter extends BaseExpandableListAdapter {

	private Context mContext = null;

	private ArrayList<HashMap<String, Object>> groupList = new ArrayList<HashMap<String, Object>>();// 存放组数据，组视图
	private ArrayList<HashMap<String, Object>> itemList = new ArrayList<HashMap<String, Object>>();// 存放子数据，子视图

	public MyExpandanleListViewAdapter(Context context,
			ArrayList<HashMap<String, Object>> list,
			ArrayList<HashMap<String, Object>> child_List) {
		this.mContext = context;
		this.groupList = list;
		this.itemList = child_List;

	}

	// 若列表中有分隔符则返回false,表明分隔符不可点
	public boolean areAllItemsEnabled() {
		return false;
	}

	/*
	 * 返回分组对象，用于一些数据传递，在事件处理时可直接取得和分组相关的数据
	 */
	public Object getGroup(int groupPosition) {
		return groupList.get(groupPosition);

	}

	/*
	 * 分组的个数
	 */
	public int getGroupCount() {
		return groupList.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/*
	 * 分组视图，这里也是一个文本视图
	 */
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		TextView text = null;
		if (convertView == null) {
			text = new TextView(mContext);
		} else {
			text = (TextView) convertView;
		}

		String name = groupList.get(groupPosition).get("categoryName")
				.toString();
		AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, 60);
		text.setLayoutParams(lp);
		text.setTextSize(18);
		text.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
		text.setPadding(48, 0, 0, 0);
		text.setText(name);
		return text;
	}

	/*
	 * 展开列表时要处理的东西都放这儿
	 */
	public void onGroupExpanded(int groupPosition) {

	}

	/*
	 * 设置子节点对象，在事件处理时返回的对象，可存放一些数据
	 */
	public Object getChild(int groupPosition, int childPosition) {
		return itemList.get(childPosition).get("instutionName").toString();
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	/*
	 * 返回当前分组的字节点个数
	 */
	public int getChildrenCount(int groupPosition) {

		return itemList.size();

	}

	/*
	 * 字节点视图，这里我们显示一个文本对象
	 */

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		TextView text = null;
		if (convertView == null) {
			text = new TextView(mContext);
		} else {
			text = (TextView) convertView;
		}
		// 获取子节点要显示的名称
		String child_name = itemList.get(childPosition).get("instutionName")
				.toString();

		// 设置文本视图的相关属性
		AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, 50);
		text.setLayoutParams(lp);
		text.setTextSize(15);
		text.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
		text.setPadding(45, 0, 0, 0);
		text.setText(child_name);
		return text;
	}

	/*
	 * 判断分组是否为空，如果为空时返回true，不为空返回flase
	 */
	public boolean isEmpty() {
		return false;
	}

	/*
	 * 收缩列表时要处理的东西都放这儿
	 */
	public void onGroupCollapsed(int groupPosition) {

	}

	/*
	 * Indicates whether the child and group IDs are stable across changes to
	 * the underlying data.
	 */
	public boolean hasStableIds() {
		return false;
	}

	/*
	 * Whether the child at the specified position is selectable.
	 */
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
