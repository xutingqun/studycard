package com.studycard.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.studycard.R;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CourseArrangementAdapter extends BaseAdapter {

	private Context context;

	private ArrayList<HashMap<String, Object>> list;

	public CourseArrangementAdapter(Context context,
			ArrayList<HashMap<String, Object>> list) {
		this.context = context;
		this.list = list;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewArrangementHolder holder;//listview�Ż�
		if (convertView == null) {

			convertView = LayoutInflater.from(context).inflate(

			R.layout.coursearrangement_item, null);

			holder = new ViewArrangementHolder();

			holder.basic_knowledge = (TextView) convertView
					.findViewById(R.id.basic_knowledge);
			holder.projet_professionnel = (TextView) convertView
					.findViewById(R.id.projet_professionnel);
			convertView.setTag(holder);
		} else {
			holder = (ViewArrangementHolder) convertView.getTag();

		}
		String basic_knowledge = list.get(position).get("basic_knowledge").toString();
		if (!TextUtils.isEmpty(basic_knowledge)) {

			holder.basic_knowledge.setText("��֪ʶ:"+basic_knowledge);
		}

		String projet_professionnel = list.get(position).get("projet_professionnel")
				.toString();
		if (!TextUtils.isEmpty(projet_professionnel)) {

			holder.projet_professionnel.setText("ʵսѵ����" + projet_professionnel);
		}
		return convertView;

	}

	class ViewArrangementHolder {
		private TextView basic_knowledge;
		private TextView projet_professionnel;
	

	}

}