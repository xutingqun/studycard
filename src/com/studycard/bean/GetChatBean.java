package com.studycard.bean;

public class GetChatBean{

	private String body;
	private String send_time;
    private int layoutID;
	
	public GetChatBean(String body, String send_time,int layoutID){
		super();
		this.send_time = send_time;
		this.body = body;
		this.layoutID = layoutID;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSend_time() {
		return send_time;
	}

	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}

	public int getLayoutID() {
		return layoutID;
	}

	public void setLayoutID(int layoutID) {
		this.layoutID = layoutID;
	}
	
	
}
