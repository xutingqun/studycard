package com.studycard.bean;

import java.io.Serializable;


public class CategoryBean implements Serializable{
	private String instutionName;
	private String uuid;
	private String description;
	//上发生发送到发送到发生
	public String getInstutionName() {
		return instutionName;
	}
	public void setInstutionName(String instutionName) {
		this.instutionName = instutionName;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}