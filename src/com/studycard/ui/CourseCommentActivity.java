package com.studycard.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.example.studycard.R;
import com.studycard.adapter.CourseCommentAdapter;
import com.avoscloud.chat.adapter.AddFriendAdapter;
import com.avoscloud.chat.service.AddRequestService;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.ui.activity.BaseActivity;
import com.avoscloud.chat.ui.view.xlist.XListView;
import com.avoscloud.chat.util.ChatUtils;
import com.avoscloud.chat.util.NetAsyncTask;
import com.avoscloud.chat.util.Utils;
import java.util.ArrayList;
import java.util.List;

public class CourseCommentActivity extends BaseActivity implements
		OnClickListener, XListView.IXListViewListener,
		AddFriendAdapter.ClickListener {
	private ImageView want_comment;// 我要评分的按钮
	private TextView all_comment;// 所有评论文案
	private String subjectId;
	private int comment_length;
	private List<AVObject> comment_list = new ArrayList<AVObject>();
	private XListView listView;
	private CourseCommentAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.coursecomment);
		initView();// 导入所需的布局控件
		Search(subjectId);
		setOnclickListenner();
	}

	// 导入所需的布局控件
	private void initView() {
		// initActionBar(App.ctx.getString(R.string.findFriends));
		want_comment = (ImageView) findViewById(R.id.want_comment);
		all_comment = (TextView) findViewById(R.id.all_comment);

		SharedPreferences preferences = getSharedPreferences("subject",
				Context.MODE_PRIVATE);
		subjectId = preferences.getString("uuid", null);

		initXListView();// 下拉刷新
	}

	public void setOnclickListenner() {
		want_comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(CourseCommentActivity.this,
						DeclinedCommentActivity.class);
				startActivity(intent);
			}

		});
	}

	private void initXListView() {
		listView = (XListView) findViewById(R.id.lv_comment);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(false);
		listView.setXListViewListener(this);
		adapter = new CourseCommentAdapter(CourseCommentActivity.this,
				comment_list);

		listView.setAdapter(adapter);
	}

	private void Search(final String subjectId) {
		new NetAsyncTask(ctx, false) {
			List<AVObject> comment_list;

			@Override
			protected void doInBack() throws Exception {
				comment_list = UserService.searchComment(subjectId,
						adapter.getCount());
				comment_length = UserService.commentCount(subjectId);

			}

			@Override
			protected void onPost(Exception e) {
				stopLoadMore();
				if (e != null) {
					e.printStackTrace();
					Utils.toast(ctx, R.string.pleaseCheckNetwork);
				} else {
					ChatUtils.handleListResult(listView, adapter, comment_list);
					all_comment.setText("所有评论（" + comment_length + "）");

				}
			}
		}.execute();

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		Search(subjectId);
	}

	private void stopLoadMore() {
		if (listView.getPullLoading()) {
			listView.stopLoadMore();
		}
	}

	@Override
	public void onAddButtonClick(final AVUser user) {
		AddRequestService.createAddRequestInBackground(this, user);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
