package com.studycard.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avoscloud.chat.base.App;
import com.avoscloud.chat.ui.activity.BaseActivity;
import com.example.studycard.R;
import com.studycard.adapter.SubjectManageAdapter;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class SubjectManageActivity extends BaseActivity {
	private ListView lv_subject;
	private ArrayList<HashMap<String, Object>> childSubject = new ArrayList<HashMap<String, Object>>();// ��ExpandListChildSubject�����������
	private ArrayList<HashMap<String, Object>> subjectManageList = new ArrayList<HashMap<String, Object>>();;
	private TextView description, cname;
	private int childPosition;
	private ProgressDialog progressDialog;
	private String subjectName, subjectDescription,uuid;
	private SubjectManageAdapter subjectManageAdapter;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.subjectmanage);
		findViewById();

		getIntentData();
		progressDialog = ProgressDialog.show(SubjectManageActivity.this, "正在加载数据，请稍后","", true, false);
		getData();

		lv_subject.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putExtra("position", position);
				intent.putExtra("list", subjectManageList);
				intent.putExtra("subjectName", subjectName);
				intent.putExtra("subjectDescription", subjectDescription);
				intent.setClass(SubjectManageActivity.this, BookCourseActivity.class);
				startActivity(intent);

				SharedPreferences preferences = getSharedPreferences("subject",
						Context.MODE_PRIVATE);
				Editor editor = preferences.edit();
				editor.putString("course_arrangement",
						subjectManageList.get(position).get("course_arrangement").toString());
				editor.putString("uuid",
						subjectManageList.get(position).get("uuid").toString());
				editor.putString("subjectName",subjectName);
				editor.commit();
			}
		});
	}

	public void findViewById() {
		initActionBar(App.ctx.getString(R.string.coursedetail));
		lv_subject = (ListView) findViewById(R.id.lv_subject);
		lv_subject.setEmptyView(findViewById(R.id.myTexts));
		description = (TextView) findViewById(R.id.description);
		cname = (TextView) findViewById(R.id.cname);
	}

	// 获取从其他界面传过来的值
	public void getIntentData() {
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		Serializable s = bundle.getSerializable("categoryList");
		childSubject = (ArrayList<HashMap<String, Object>>) s;
		childPosition = bundle.getInt("childPosition");
		subjectName = childSubject.get(childPosition).get("instutionName")
				.toString();
		 uuid = childSubject.get(childPosition).get("uuid").toString();
		cname.setText(subjectName);
		subjectDescription = childSubject.get(childPosition).get("description")
				.toString();
		description.setText(subjectDescription);
	}

	public void getData() {
		AVQuery<AVObject> avQuery = new AVQuery<AVObject>("subject");
		avQuery.whereEqualTo("categorysubject_id", uuid);
		avQuery.findInBackground(new FindCallback<AVObject>() {

			@Override
			public void done(List<AVObject> arg0, AVException arg1) {
				for (int i = 0; i < arg0.size(); i++) {
					AVObject avObject = arg0.get(i);
					HashMap<String, Object> hashMap = new HashMap<String, Object>();

					hashMap.put("teacher",
							avObject.getString("teacher"));
					hashMap.put("teacher_description",
							avObject.getString("teacher_description"));
					hashMap.put("uuid", avObject.getObjectId());
					hashMap.put("course_arrangement",
							avObject.getString("course_arrangement"));
					hashMap.put("place", avObject.getString("place"));
					hashMap.put("schooltime", avObject.getString("schooltime"));

					subjectManageList.add(hashMap);
				}
				subjectManageAdapter = new SubjectManageAdapter(
						SubjectManageActivity.this, subjectManageList);

				lv_subject.setAdapter(subjectManageAdapter);
				progressDialog.dismiss();
			}

		});
	}

	
	
}
