package com.studycard.ui;

import com.example.studycard.R;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class CourseArrangementActivity extends Activity {

	private TextView lv_arrangement;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.coursearrangement);

		init();

	}

	// 导入所需布局和布局控件并给相关布局set值。
	public void init() {
		lv_arrangement = (TextView) findViewById(R.id.lv_arrangement);
		SharedPreferences preferences = getSharedPreferences("subject",
				Context.MODE_PRIVATE);
		String course_arrangement = preferences.getString("course_arrangement",
				null);
		lv_arrangement.setText(course_arrangement);

	}
}
