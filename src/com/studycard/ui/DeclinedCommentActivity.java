package com.studycard.ui;

import java.sql.Date;
import java.text.SimpleDateFormat;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.avoscloud.chat.base.App;
import com.avoscloud.chat.ui.activity.BaseActivity;
import com.example.studycard.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class DeclinedCommentActivity extends BaseActivity {
	private RatingBar rt_class, rt_high, rt_teacher;// 评分条
	private EditText input;// 评论内容输入框
	private Button input_button, score_button;// 发送按钮，保存分数按钮
	private TextView pinjun;// 平均分
	private float class_score, high_score, teacher_score,average;
	private String subjectId, username, comment_body,str;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ratingbar);

		findViewById();// 导入所需布局
		// 点击计算平均分
		score_button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				class_score = rt_class.getRating();
				high_score = rt_high.getRating();
				teacher_score = rt_teacher.getRating();
				average = (class_score + high_score + teacher_score) / 3;
				pinjun.setText("所给平均分为：" + average + "分");
				if (average == 0) {
					Toast.makeText(DeclinedCommentActivity.this, "请先评分",
							Toast.LENGTH_LONG).show();
				}
			}

		});
		// 发送按钮
		input_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				init();
				if (comment_body == null) {
					Toast.makeText(DeclinedCommentActivity.this, "请先填写评价内容",
							Toast.LENGTH_LONG).show();
				} else {
					saveData();
				}

			}

		});
	}

	public void init() {
		SharedPreferences preferences = getSharedPreferences("subject",
				Context.MODE_PRIVATE);
		subjectId = preferences.getString("uuid", null);
		username = AVUser.getCurrentUser().getUsername();
		comment_body = input.getText().toString().trim();
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
	    str = formatter.format(curDate);

	}

	public void findViewById() {
		initActionBar(App.ctx.getString(R.string.coursecomment));
		rt_class = (RatingBar) findViewById(R.id.rt_class);
		rt_high = (RatingBar) findViewById(R.id.rt_high);
		rt_teacher = (RatingBar) findViewById(R.id.rt_teacher);
		input = (EditText) findViewById(R.id.input);
		pinjun = (TextView) findViewById(R.id.pinjun);
		input_button = (Button) findViewById(R.id.input_button);
		score_button = (Button) findViewById(R.id.score_button);
	}

	// 将数据（评分和评论内容）保存到leancloud
	public void saveData() {
		AVObject comment = new AVObject("comment");// 括号里的comment是leancloud对应的表名
		comment.put("comment_id", subjectId);// 将subjectId存到表comment中的comment_id属性里
		comment.put("comment_body", comment_body);// 将comment_body(即从输入框获取的用户输入的内容）存到表comment中的comment_body中
		comment.put("comment_time", str);
		comment.put("comment_score", average);// 原理同上
		comment.put("comment_name", username);
		comment.saveInBackground(new SaveCallback() {// 在后台保存数据

			@Override
			public void done(AVException arg0) {
				// TODO Auto-generated method stub
				if (arg0 == null) {// arg0表示错误，这里错误为0，即没有报错，也就是说保存数据到leancloud成功
					Toast.makeText(DeclinedCommentActivity.this, "发送成功",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(DeclinedCommentActivity.this, "发送失败",
							Toast.LENGTH_LONG).show();
				}
			}
		});

	}
}
