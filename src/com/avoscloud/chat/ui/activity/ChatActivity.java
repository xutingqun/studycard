package com.avoscloud.chat.ui.activity;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMLocationMessage;
import com.example.studycard.R;
import com.avoscloud.chat.adapter.ChatMsgAdapter;
import com.avoscloud.chat.adapter.EmotionGridAdapter;
import com.avoscloud.chat.adapter.EmotionPagerAdapter;
import com.avoscloud.chat.db.MsgsTable;
import com.avoscloud.chat.db.RoomsTable;
import com.avoscloud.chat.entity.AVIMUserInfoMessage;
import com.avoscloud.chat.entity.ConvType;
import com.avoscloud.chat.service.CacheService;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.service.chat.ConvManager;
import com.avoscloud.chat.service.chat.IM;
import com.avoscloud.chat.service.chat.MsgAgent;
import com.avoscloud.chat.service.chat.MsgUtils;
import com.avoscloud.chat.service.emoji.EmotionUtils;
import com.avoscloud.chat.service.event.ConvChangeEvent;
import com.avoscloud.chat.service.event.FinishEvent;
import com.avoscloud.chat.service.event.MsgEvent;
import com.avoscloud.chat.ui.view.EmotionEditText;
import com.avoscloud.chat.ui.view.RecordButton;
import com.avoscloud.chat.ui.view.xlist.XListView;
import com.avoscloud.chat.util.*;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatActivity extends ConvBaseActivity implements OnClickListener,
		XListView.IXListViewListener {
	public static final int LOCATION_REQUEST = 1;
	public static final int PAGE_SIZE = 20;
	public static final String CONVID = "convid";
	private static final int TAKE_CAMERA_REQUEST = 2;
	private static final int GALLERY_REQUEST = 0;
	private static final int GALLERY_KITKAT_REQUEST = 3;
	public static Activity ctx;
	public static ChatActivity instance;
	private ChatMsgAdapter adapter;
	private RoomsTable roomsTable;
	private boolean visible;

	private View chatTextLayout, chatAudioLayout, chatAddLayout,
			chatEmotionLayout;
	private View turnToTextBtn, turnToAudioBtn, sendBtn, addImageBtn,
			showAddBtn, addLocationBtn, showEmotionBtn;
	private ViewPager emotionPager;
	private EmotionEditText contentEdit;
	private XListView xListView;
	private RecordButton recordBtn;
	private String localCameraPath = PathUtils.getTmpPath();
	private View addCameraBtn;
	private ConvType convType;
	private AVIMConversation conv;
	private MsgsTable msgsTable;
	private MsgAgent msgAgent;
	private MsgAgent.SendCallback defaultSendCallback = new DefaultSendCallback();

	// 从会话界面直接跳转到聊天界面，通过会话信息获取该会话的id，然后跳转到聊天界面，并将所获取到会话信息传到该页面
	public static void goByConv(Context from, AVIMConversation conv) {
		CacheService.registerConv(conv);
		Intent intent = new Intent(from, ChatActivity.class);
		intent.putExtra(CONVID, conv.getConversationId());
		from.startActivity(intent);
	}

	// 若是从好友列表直接向好友发起会话
	public static void goByUserId(final Activity from, String userId) {
		final ProgressDialog dialog = Utils.showSpinnerDialog(from);// 显示正在加载的进度条
		ConvManager.getInstance().fetchConvWithUserId(userId,
				new AVIMConversationCreatedCallback() {
					@Override
					public void done(AVIMConversation conversation,
							AVException e) {
						dialog.dismiss();
						if (Utils.filterException(e)) {
							goByConv(from, conversation);
						}
					}
				});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_layout);
		commonInit();
		findView();
		initEmotionPager();// 使用viewpage和gridview实现表情分页显示
		initRecordBtn();
		setEditTextChangeListener();
		initListView();
		setSoftInputMode();//调用父类隐藏软键盘的方法
		initByIntent(getIntent());
	}

	private void testSendCustomMessage() {
		AVIMUserInfoMessage userInfoMessage = new AVIMUserInfoMessage();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("nickname", "lzwjava");
		userInfoMessage.setAttrs(map);
		conv.sendMessage(userInfoMessage, new AVIMConversationCallback() {
			@Override
			public void done(AVException e) {
				if (e != null) {
					Logger.d(e.getMessage());
				}
			}
		});
	}

	private void initByIntent(Intent intent) {
		initData(intent);
		refreshMsgsFromDB();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		initByIntent(intent);
	}

	// 下拉刷新和上拉加载的工具类
	private void initListView() {
		xListView.setPullRefreshEnable(true);
		xListView.setPullLoadEnable(false);
		xListView.setXListViewListener(this);
		xListView.setOnScrollListener(new PauseOnScrollListener(
				UserService.imageLoader, true, true));
	}

	private void initEmotionPager() {
		List<View> views = new ArrayList<View>();
		for (int i = 0; i < EmotionUtils.emojiGroups.size(); i++) {
			views.add(getEmotionGridView(i));
		}
		EmotionPagerAdapter pagerAdapter = new EmotionPagerAdapter(views);
		// 设置缓存页数，避免加载耗时影响用户体验
		emotionPager.setOffscreenPageLimit(3);
		emotionPager.setAdapter(pagerAdapter);
	}

	private View getEmotionGridView(int pos) {
		LayoutInflater inflater = LayoutInflater.from(ctx);
		View emotionView = inflater.inflate(R.layout.chat_emotion_gridview,
				null, false);
		GridView gridView = (GridView) emotionView.findViewById(R.id.gridview);// 用gridView来实现图片的分布式排列
		final EmotionGridAdapter emotionGridAdapter = new EmotionGridAdapter(
				ctx);
		List<String> pageEmotions = EmotionUtils.emojiGroups.get(pos);// pos表示第几页
		emotionGridAdapter.setDatas(pageEmotions);
		gridView.setAdapter(emotionGridAdapter);
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String emotionText = (String) parent.getAdapter().getItem(
						position);// 根据点击位置获取所选图片的文本信息，如：smile:
				int start = contentEdit.getSelectionStart();
				StringBuffer sb = new StringBuffer(contentEdit.getText());
				// 调用EmotionEditText中的repalce方法，将文字换成表情
				sb.replace(contentEdit.getSelectionStart(),
						contentEdit.getSelectionEnd(), emotionText);
				contentEdit.setText(sb.toString());

				CharSequence info = contentEdit.getText();
				if (info instanceof Spannable) {
					Spannable spannable = (Spannable) info;
					Selection.setSelection(spannable,
							start + emotionText.length());
				}
			}
		});
		return gridView;
	}

	// 处理录音
	public void initRecordBtn() {
		recordBtn.setSavePath(PathUtils.getRecordTmpPath());// 设置音频的保存路径
		recordBtn
				.setRecordEventListener(new RecordButton.RecordEventListener() {
					@Override
					public void onFinishedRecord(final String audioPath,
							int secs) {
						msgAgent.sendAudio(audioPath);
					}

					@Override
					public void onStartRecord() {

					}
				});
	}

	public void setEditTextChangeListener() {
		contentEdit.addTextChangedListener(new SimpleTextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > 0) {
					sendBtn.setEnabled(true);// 发送按钮可用
					showSendBtn();
				} else {
					sendBtn.setEnabled(false);// 发送按钮不可用
					showTurnToRecordBtn();
				}
				super.onTextChanged(s, start, before, count);
			}
		});
	}

	private void showTurnToRecordBtn() {
		sendBtn.setVisibility(View.GONE);// 设置发送按钮不可见
		turnToAudioBtn.setVisibility(View.VISIBLE);
	}

	private void showSendBtn() {
		sendBtn.setVisibility(View.VISIBLE);// 设置发送按钮控件可见
		turnToAudioBtn.setVisibility(View.GONE);
	}

	private void findView() {
		xListView = (XListView) findViewById(R.id.listview);// 显示聊天内容的listview
		addImageBtn = findViewById(R.id.addImageBtn);// 发送照片的按钮

		contentEdit = (EmotionEditText) findViewById(R.id.textEdit);// 该文本框是一个可以图文并茂的文本框
		chatTextLayout = findViewById(R.id.chatTextLayout);
		chatAudioLayout = findViewById(R.id.chatRecordLayout);// 录音的layout
		turnToAudioBtn = findViewById(R.id.turnToAudioBtn);
		turnToTextBtn = findViewById(R.id.turnToTextBtn);
		recordBtn = (RecordButton) findViewById(R.id.recordBtn);
		chatTextLayout = findViewById(R.id.chatTextLayout);
		chatAddLayout = findViewById(R.id.chatAddLayout);
		addLocationBtn = findViewById(R.id.addLocationBtn);
		chatEmotionLayout = findViewById(R.id.chatEmotionLayout);
		showAddBtn = findViewById(R.id.showAddBtn);
		showEmotionBtn = findViewById(R.id.showEmotionBtn);
		sendBtn = findViewById(R.id.sendBtn);
		emotionPager = (ViewPager) findViewById(R.id.emotionPager);
		addCameraBtn = findViewById(R.id.addCameraBtn);

		sendBtn.setOnClickListener(this);// 监听发送按钮
		contentEdit.setOnClickListener(this);// 内容编辑监听
		addImageBtn.setOnClickListener(this);// 发送图片的监听
		addLocationBtn.setOnClickListener(this);// 发送位置的监听
		turnToAudioBtn.setOnClickListener(this);// 转向录音按钮监听
		turnToTextBtn.setOnClickListener(this);// 转向文本编辑按钮
		showAddBtn.setOnClickListener(this);// 其他更多选择的按钮监听
		showEmotionBtn.setOnClickListener(this);// 显示表情的按钮监听
		addCameraBtn.setOnClickListener(this);// 调用摄像头拍照按钮监听
	}

	void commonInit() {
		ctx = this;
		instance = this;
		msgsTable = MsgsTable.getCurrentUserInstance();
		roomsTable = RoomsTable.getCurrentUserInstance();
	}

	public void initData(Intent intent) {
		String convid = intent.getStringExtra(CONVID);
		conv = CacheService.lookupConv(convid);
		if (conv == null) {
			throw new NullPointerException("conv is null");
		}
		initActionBar(ConvManager.titleOfConv(conv));
		msgAgent = new MsgAgent(conv);
		msgAgent.setSendCallback(defaultSendCallback);
		CacheService.setCurConv(conv);
		roomsTable.insertRoom(convid);
		roomsTable.clearUnread(conv.getConversationId());
		convType = ConvManager.typeOfConv(conv);

		bindAdapterToListView(convType);
	}

	private void bindAdapterToListView(ConvType convType) {
		adapter = new ChatMsgAdapter(this, convType);
		adapter.setClickListener(new ChatMsgAdapter.ClickListener() {
			@Override
			public void onFailButtonClick(AVIMTypedMessage msg) {
				msgAgent.resendMsg(msg, defaultSendCallback);
			}

			@Override
			public void onLocationViewClick(AVIMLocationMessage locMsg) {
				Intent intent = new Intent(ctx, LocationActivity.class);
				intent.putExtra(LocationActivity.TYPE,
						LocationActivity.TYPE_SCAN);
				intent.putExtra(LocationActivity.LATITUDE, locMsg.getLocation()
						.getLatitude());
				intent.putExtra(LocationActivity.LONGITUDE, locMsg
						.getLocation().getLongitude());
				ctx.startActivity(intent);
			}

			@Override
			public void onImageViewClick(AVIMImageMessage imageMsg) {
				ImageBrowserActivity.go(ChatActivity.this,
						MsgUtils.getFilePath(imageMsg), imageMsg.getFileUrl());
			}
		});
		xListView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.chat_ativity_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int menuId = item.getItemId();
		if (menuId == R.id.people) {
			Utils.goActivity(ctx, ConvDetailActivity.class);
		}
		return super.onMenuItemSelected(featureId, item);
	}

	public void refreshMsgsFromDB() {
		new GetDataTask(ctx, false).execute();
	}

	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				new GetDataTask(ctx, true).execute();
			}
		}, 1000);
	}

	@Override
	public void onLoadMore() {
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.sendBtn:
			sendText();
			break;
		case R.id.addImageBtn:
			selectImageFromLocal();
			break;
		case R.id.turnToAudioBtn:
			showAudioLayout();
			break;
		case R.id.turnToTextBtn:
			showTextLayout();
			break;
		case R.id.showAddBtn:
			toggleBottomAddLayout();
			break;
		case R.id.showEmotionBtn:
			toggleEmotionLayout();
			break;
		case R.id.addLocationBtn:
			selectLocationFromMap();
			break;
		case R.id.textEdit:
			hideBottomLayoutAndScrollToLast();
			break;
		case R.id.addCameraBtn:
			selectImageFromCamera();
			break;
		}
	}

	private void hideBottomLayoutAndScrollToLast() {
		hideBottomLayout();
		scrollToLast();
	}

	private void hideBottomLayout() {
		hideAddLayout();
		chatEmotionLayout.setVisibility(View.GONE);
	}

	private void selectLocationFromMap() {
		Intent intent = new Intent(this, LocationActivity.class);
		intent.putExtra(LocationActivity.TYPE, LocationActivity.TYPE_SELECT);
		startActivityForResult(intent, LOCATION_REQUEST);
	}

	private void toggleEmotionLayout() {
		if (chatEmotionLayout.getVisibility() == View.VISIBLE) {
			chatEmotionLayout.setVisibility(View.GONE);
		} else {
			chatEmotionLayout.setVisibility(View.VISIBLE);
			hideAddLayout();
			showTextLayout();
			hideSoftInputView();
		}
	}

	private void toggleBottomAddLayout() {
		if (chatAddLayout.getVisibility() == View.VISIBLE) {
			hideAddLayout();
		} else {
			chatEmotionLayout.setVisibility(View.GONE);
			hideSoftInputView();
			showAddLayout();
		}
	}

	private void hideAddLayout() {
		chatAddLayout.setVisibility(View.GONE);
	}

	private void showAddLayout() {
		chatAddLayout.setVisibility(View.VISIBLE);
	}

	private void showTextLayout() {
		chatTextLayout.setVisibility(View.VISIBLE);
		chatAudioLayout.setVisibility(View.GONE);
	}

	private void showAudioLayout() {
		chatTextLayout.setVisibility(View.GONE);
		chatAudioLayout.setVisibility(View.VISIBLE);
		chatEmotionLayout.setVisibility(View.GONE);
		hideSoftInputView();
	}

	// 从本地获取图片
	public void selectImageFromLocal() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent,
							getResources().getString(R.string.select_picture)),
					GALLERY_REQUEST);
		} else {
			Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			intent.setType("image/*");
			startActivityForResult(intent, GALLERY_KITKAT_REQUEST);
		}
	}

	public void selectImageFromCamera() {
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri imageUri = Uri.fromFile(new File(localCameraPath));
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(openCameraIntent, TAKE_CAMERA_REQUEST);
	}

	// 发送文本信息
	private void sendText() {
		final String content = contentEdit.getText().toString();// 获取输入框中的内容
		if (!TextUtils.isEmpty(content)) {
			msgAgent.sendText(content);
			contentEdit.setText("");
		}
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case GALLERY_REQUEST:
			case GALLERY_KITKAT_REQUEST:
				if (data == null) {
					Utils.toast("return data is null");
					return;
				}
				Uri uri;
				if (requestCode == GALLERY_REQUEST) {
					uri = data.getData();
				} else {
					// for Android 4.4
					uri = data.getData();
					final int takeFlags = data.getFlags()
							& (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
					getContentResolver().takePersistableUriPermission(uri,
							takeFlags);
				}
				String localSelectPath = ProviderPathUtils.getPath(ctx, uri);
				msgAgent.sendImage(localSelectPath);
				break;
			case TAKE_CAMERA_REQUEST:
				msgAgent.sendImage(localCameraPath);
				break;
			case LOCATION_REQUEST:
				final double latitude = data.getDoubleExtra(
						LocationActivity.LATITUDE, 0);
				final double longitude = data.getDoubleExtra(
						LocationActivity.LONGITUDE, 0);
				final String address = data
						.getStringExtra(LocationActivity.ADDRESS);
				if (!TextUtils.isEmpty(address)) {
					msgAgent.sendLocation(latitude, longitude, address);
				} else {
					Utils.toast(ctx, R.string.cannotGetYourAddressInfo);
				}
				break;
			}
		}
		hideBottomLayout();
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void scrollToLast() {
		xListView.post(new Runnable() {
			@Override
			public void run() {
				// fast scroll
				/*
				 * xListView.requestFocusFromTouch();
				 * xListView.setSelection(xListView.getAdapter().getCount() -
				 * 1); contentEdit.requestFocusFromTouch();
				 */
				xListView.smoothScrollToPosition(xListView.getAdapter()
						.getCount() - 1);
			}
		});

	}

	@Override
	protected void onDestroy() {
		CacheService.setCurConv(null);
		instance = null;
		super.onDestroy();
	}

	public void onEvent(MsgEvent msgEvent) {
		AVIMTypedMessage msg = msgEvent.getMsg();
		if (msg.getConversationId().equals(conv.getConversationId())) {
			roomsTable.clearUnread(conv.getConversationId());
			refreshMsgsFromDB();
		}
	}

	@Override
	public void onEvent(ConvChangeEvent convChangeEvent) {

	}

	@Override
	protected void onConvChanged(AVIMConversation conv) {
		this.conv = conv;
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(ConvManager.titleOfConv(this.conv));
	}

	@Override
	public void onEvent(FinishEvent finishEvent) {
		this.finish();
	}

	public boolean isVisible() {
		return visible;
	}

	@Override
	protected void onResume() {
		super.onResume();
		IM.getInstance().cancelNotification();
		visible = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		visible = false;
	}

	class GetDataTask extends NetAsyncTask {
		private List<AVIMTypedMessage> msgs;
		private boolean loadHistory;

		GetDataTask(Context ctx, boolean loadHistory) {
			super(ctx, false);
			this.loadHistory = loadHistory;
		}

		@Override
		protected void doInBack() throws Exception {
			String msgId = null;
			long maxTime = System.currentTimeMillis() + 10 * 1000;
			int limit;
			long time;
			if (loadHistory == false) {
				time = maxTime;
				int count = adapter.getCount();
				if (count > PAGE_SIZE) {
					limit = count;
				} else {
					limit = PAGE_SIZE;
				}
			} else {
				if (adapter.getDatas().size() > 0) {
					msgId = adapter.getDatas().get(0).getMessageId();
					AVIMTypedMessage firstMsg = adapter.getDatas().get(0);
					time = firstMsg.getTimestamp();
				} else {
					time = maxTime;
				}
				limit = PAGE_SIZE;
			}
			msgs = msgsTable.selectMsgs(conv.getConversationId(), time, limit);
			// msgs = ConvManager.getInstance().queryHistoryMessage(conv, msgId,
			// time, limit);
			CacheService.cacheMsgs(msgs);
		}

		@Override
		protected void onPost(Exception e) {
			if (Utils.filterException(e)) {
				ChatUtils.stopRefresh(xListView);
				if (loadHistory == false) {
					adapter.setDatas(msgs);
					Logger.d("msgs size=" + msgs.size());
					adapter.notifyDataSetChanged();
					scrollToLast();
				} else {
					List<AVIMTypedMessage> newMsgs = new ArrayList<AVIMTypedMessage>();
					newMsgs.addAll(msgs);
					newMsgs.addAll(adapter.getDatas());
					adapter.setDatas(newMsgs);
					adapter.notifyDataSetChanged();
					if (msgs.size() > 0) {
						xListView.setSelection(msgs.size() - 1);
					} else {
						Utils.toast(R.string.loadMessagesFinish);
					}
				}
			}
		}
	}

	class DefaultSendCallback implements MsgAgent.SendCallback {

		@Override
		public void onError(Exception e) {
			refreshMsgsFromDB();
		}

		@Override
		public void onSuccess(AVIMTypedMessage msg) {
			refreshMsgsFromDB();
		}
	}
}
