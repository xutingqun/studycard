package com.avoscloud.chat.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.example.studycard.R;
import com.avoscloud.chat.adapter.GroupAddMembersAdapter;
import com.avoscloud.chat.entity.ConvType;
import com.avoscloud.chat.service.CacheService;
import com.avoscloud.chat.service.chat.ConvManager;
import com.avoscloud.chat.service.event.FinishEvent;
import com.avoscloud.chat.util.Utils;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 邀请群成员
 */
public class ConvAddMembersActivity extends ConvBaseActivity {
	public static final int OK = 0;
	private GroupAddMembersAdapter adapter;
	private ListView userList;
	private ConvManager convManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_add_members_layout);
		findView();// 导入所需的布局控件
		convManager = ConvManager.getInstance();// 使用单例模式创建类的实例，即一个类有且只有一个实例
		initList();// 数据处理
		initActionBar();// 页面头部title初始化
		setListData();
	}

	@Override
	protected void onConvChanged(AVIMConversation conv) {
	}

	private void setListData() {
		List<String> ids = new ArrayList<String>();
		ids.addAll(CacheService.getFriendIds());// 获取用户的所有好友id并将其存放在ids中
		ids.removeAll(conv().getMembers());// 去掉已经在会话中的好友id
		adapter.setDatas(ids);
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuItem add = menu.add(0, OK, 0, R.string.sure);
		alwaysShowMenuItem(add);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int id = item.getItemId();
		if (id == OK) {
			addMembers();
		}
		return super.onMenuItemSelected(featureId, item);
	}

	// 添加成员到会话中
	private void addMembers() {
		final List<String> checkedUsers = adapter.getCheckedDatas();//获取被邀请的成员数据
		final ProgressDialog dialog = Utils.showSpinnerDialog(this);
		if (checkedUsers.size() == 0) {//若无新邀请的成员则结束跳出
			finish();
		} else {
			if (ConvManager.typeOfConv(conv()) == ConvType.Single) {//如果原对话是单聊
				List<String> members = new ArrayList<String>();
				members.addAll(checkedUsers);//将新邀请的成员存在members中
				members.addAll(conv().getMembers());//将当前已在聊天中的成员也存入members中
				//创建群组会话
				convManager.createGroupConv(members,
						new AVIMConversationCreatedCallback() {
							@Override
							public void done(
									final AVIMConversation conversation,
									AVException e) {
								if (Utils.filterException(e)) {
									EventBus eventBus = EventBus.getDefault();//使用默认的单例实例化实例化EventBus对象
									FinishEvent finishEvent = new FinishEvent();
									eventBus.post(finishEvent);//向前一个Activity通知事件结束
									//跳转到会话界面
									ChatActivity.goByConv(
											ConvAddMembersActivity.this,
											conversation);
								}
							}
						});
			} else {
				conv().addMembers(checkedUsers, new AVIMConversationCallback() {
					@Override
					public void done(AVException e) {
						dialog.dismiss();
						if (Utils.filterException(e)) {
							Utils.toast(R.string.inviteSucceed);
							convManager.postConvChanged(conv());
							finish();
						}
					}
				});
			}
		}
	}

	private void initList() {
		adapter = new GroupAddMembersAdapter(ctx, new ArrayList<String>());
		userList.setAdapter(adapter);
	}

	private void findView() {
		userList = (ListView) findViewById(R.id.userList);
	}

}
