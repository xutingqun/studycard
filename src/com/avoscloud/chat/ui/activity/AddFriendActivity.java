package com.avoscloud.chat.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.avos.avoscloud.AVUser;
import com.example.studycard.R;
import com.avoscloud.chat.adapter.AddFriendAdapter;
import com.avoscloud.chat.base.App;
import com.avoscloud.chat.service.AddRequestService;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.ui.view.xlist.XListView;
import com.avoscloud.chat.util.ChatUtils;
import com.avoscloud.chat.util.NetAsyncTask;
import com.avoscloud.chat.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddFriendActivity extends BaseActivity implements OnClickListener,
		XListView.IXListViewListener, OnItemClickListener,
		AddFriendAdapter.ClickListener {
	EditText searchNameEdit;// 输入查询条件的输入框
	Button searchBtn;// 搜索框按钮
	List<AVUser> users = new ArrayList<AVUser>();// 存放搜素到的用户
	XListView listView;
	AddFriendAdapter adapter;
	String searchName = "";// 输入框输入的内容

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_add_friend_activity);
		initView();// 导入所需的布局控件
		search(searchName);// 异步查询
	}

	// 导入所需的布局控件
	private void initView() {
		initActionBar(App.ctx.getString(R.string.findFriends));
		searchNameEdit = (EditText) findViewById(R.id.searchNameEdit);
		searchBtn = (Button) findViewById(R.id.searchBtn);
		searchBtn.setOnClickListener(this);
		initXListView();// 下拉刷新
	}

	private void initXListView() {
		listView = (XListView) findViewById(R.id.searchList);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(false);
		listView.setXListViewListener(this);

		adapter = new AddFriendAdapter(this, users);
		adapter.setClickListener(this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.searchBtn:
			searchName = searchNameEdit.getText().toString();
			if (searchName != null) {
				adapter.clear();
				search(searchName);
			}
			break;
		}
	}

	/*
	 * 开启异步线程，根据输入框内容去数据库进行模糊匹配并将符合条件的数据取出来 在doInBack中执行比较耗时的操作，如查询数据的过程
	 * 在doInBack中获取的结果在onPost中处理
	 */
	private void search(final String searchName) {
		new NetAsyncTask(ctx, false) {
			List<AVUser> users;

			@Override
			protected void doInBack() throws Exception {
				users = UserService.searchUser(searchName, adapter.getCount());
			}

			@Override
			protected void onPost(Exception e) {
				stopLoadMore();
				if (e != null) {
					e.printStackTrace();
					Utils.toast(ctx, R.string.pleaseCheckNetwork);
				} else {
					ChatUtils.handleListResult(listView, adapter, users);
				}
			}
		}.execute();

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		search(searchName);
	}

	private void stopLoadMore() {
		if (listView.getPullLoading()) {
			listView.stopLoadMore();
		}
	}

	/*
	 * 点击添加时创建一个请求
	 * 获取当前用户和想要添加的用户，并给添加设置一个状态（等待验证）
	 * 先去判断该添加请求是否已存在，若存在，则给用户一个“已经请求过了”的提示
	 * 若还未添加则创建该添加请求并将其保存到数据库的AddRequest表中
	 */
	@Override
	public void onAddButtonClick(final AVUser user) {
		AddRequestService.createAddRequestInBackground(this, user);
	}
}
