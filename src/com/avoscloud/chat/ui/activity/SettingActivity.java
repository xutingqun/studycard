package com.avoscloud.chat.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;
import com.example.studycard.R;
import com.avoscloud.chat.db.DBHelper;
import com.avoscloud.chat.service.chat.IM;
import com.avoscloud.chat.util.*;

/**
 * Created by lzw on 14-9-17.
 */
public class SettingActivity extends BaseActivity implements
		View.OnClickListener {
	private static final int IMAGE_PICK_REQUEST = 1;
	private static final int CROP_REQUEST = 2;
	TextView usernameView, genderView;
	ImageView avatarView;
	View messagesetLayout, aboutusLayout, logoutLayout, notifyLayout,
			updateLayout;
	IM im;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		im = IM.getInstance();
		findView();// 导入布局
	}

	private void findView() {
		initActionBar(R.string.setting);

		messagesetLayout = findViewById(R.id.messageset_layout);
		aboutusLayout = findViewById(R.id.aboutus_layout);
		updateLayout = findViewById(R.id.updateLayout);
		logoutLayout = findViewById(R.id.logoutLayout);

		messagesetLayout.setOnClickListener(this);
		aboutusLayout.setOnClickListener(this);
		updateLayout.setOnClickListener(this);
		logoutLayout.setOnClickListener(this);
	}

	// 监听不同按钮执行不同操作
	@Override
	public void onClick(View v) {
		int id = v.getId();
		// 若是更换头像则调用系统相册并返回一个标志IMAGE_PICK_REQUEST，请求码
		if (id == R.id.logoutLayout) {// 退出应用
			im.close();
			DBHelper.getCurrentUserInstance(ctx).closeHelper();
			AVUser.logOut();
		    Intent intent = new Intent(ctx, EntryLoginActivity.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		    ctx.startActivity(intent);
//			Utils.goActivity(ctx, EntryLoginActivity.class);// 用户注销之后回到登录界面

		} else if (id == R.id.messageset_layout) {
			Utils.goActivity(ctx, NotifySettingActivity.class);
		} else if (id == R.id.updateLayout) {// 检查更新
		// UpdateService updateService = UpdateService
		// .getInstance(getActivity());
		// updateService.showSureUpdateDialog();
		} else if (id == R.id.aboutus_layout) {
			Utils.goActivity(ctx, AboutStudyCardActivity.class);
		} 
		
	}

}
