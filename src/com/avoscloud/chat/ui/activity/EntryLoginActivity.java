package com.avoscloud.chat.ui.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.avos.avoscloud.AVUser;
import com.example.studycard.R;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.util.NetAsyncTask;
import com.avoscloud.chat.util.Utils;

public class EntryLoginActivity extends EntryBaseActivity implements
		OnClickListener {
	EditText usernameEdit, passwordEdit;// 输入框中的用户名和密码
	Button loginBtn;// 登录按钮
	TextView registerBtn;// 注册按钮

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entry_login_activity);
		init();// 导入布局并给按钮设置监听
	}

	// 导入布局并给按钮设置监听
	private void init() {

		usernameEdit = (EditText) findViewById(R.id.et_username);
		passwordEdit = (EditText) findViewById(R.id.et_password);
		loginBtn = (Button) findViewById(R.id.btn_login);
		registerBtn = (TextView) findViewById(R.id.btn_register);
		loginBtn.setOnClickListener(this);
		registerBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == registerBtn) {
			Utils.goActivity(ctx, EntryRegisterActivity.class);
		} else {
			login();
		}
	}

	private void login() {
		// 获取输入框中的用户名和密码
		final String name = usernameEdit.getText().toString();
		final String password = passwordEdit.getText().toString();

		// 若用户名为空则提示不能为空
		if (TextUtils.isEmpty(name)) {
			Utils.toast(R.string.username_cannot_null);
			return;
		}
		// 若用户名为空则提示不能为空
		if (TextUtils.isEmpty(password)) {
			Utils.toast(R.string.password_can_not_null);
			return;
		}

		new NetAsyncTask(ctx) {
			/*
			 * (non-Javadoc)
			 * @see com.avoscloud.chat.util.NetAsyncTask#doInBack()
			 * AsyncTask的doInBack（）方法是 后台执行，比较耗时的操作都可以放在这里。注意这里不能直接操作UI
			 * 在onPost使用在doInBack 得到的结果处理操作UI。 此方法在主线程执行，任务执行的结果作为此方法的参数返回
			 */
			@Override
			protected void doInBack() throws Exception {
				AVUser.logIn(name, password);
			}

			@Override
			protected void onPost(Exception e) {
				if (e != null) {
					Utils.toast(e.getMessage());
				} else {
					UserService.updateUserLocation();
					MainActivity.goMainActivity(EntryLoginActivity.this);
				}
			}
		}.execute();

	}
}
