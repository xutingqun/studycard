package com.avoscloud.chat.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.avoscloud.chat.avobject.User;
import com.avoscloud.chat.service.UserService;
import com.avoscloud.chat.service.chat.IM;
import com.avoscloud.chat.ui.activity.InformationActivity;
import com.avoscloud.chat.ui.activity.PersonalInfoSettingActivity;
import com.avoscloud.chat.ui.activity.SettingActivity;
import com.avoscloud.chat.util.*;
import com.example.studycard.R;
import com.studycard.ui.MyCourseActivity;

/**
 * Created by lzw on 14-9-17.
 */
public class MySpaceFragment extends BaseFragment implements
		View.OnClickListener {
	TextView usernameView;//用户名
	ImageView image_avatars;//用户头像
	View settingLayout, changeuserinfoLayout, courseinfoLayout, newsinfoLayout;
	IM im;//设置按钮、修改用户资料按钮、我的课程表按钮、最新资讯按钮
	// 修改信息返回时保存并刷新界面
	SaveCallback saveCallback = new SaveCallback() {
		@Override
		public void done(AVException e) {
			refresh();
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.my_space_fragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// 设置标题
		headerLayout.showTitle(R.string.me);//头部标题
		findView();// 导入布局
		refresh();// 获取用户信息
	}

	private void refresh() {
		AVUser curUser = AVUser.getCurrentUser();
		usernameView.setText(curUser.getUsername());// 显示用户名
		UserService.displayAvatar(User.getAvatarUrl(curUser), image_avatars);// 设置用户头像
	}

	private void findView() {
		View fragmentView = getView();
		usernameView = (TextView) fragmentView.findViewById(R.id.avatar_name);
		image_avatars = (ImageView) fragmentView.findViewById(R.id.image_avatars);
		settingLayout = fragmentView.findViewById(R.id.settinginfo);
		changeuserinfoLayout = fragmentView.findViewById(R.id.changeuserinfo);
		courseinfoLayout = fragmentView.findViewById(R.id.courseinfo);
		newsinfoLayout = fragmentView.findViewById(R.id.newsinfo);

		changeuserinfoLayout.setOnClickListener(this);
		courseinfoLayout.setOnClickListener(this);
		newsinfoLayout.setOnClickListener(this);
		settingLayout.setOnClickListener(this);
		newsinfoLayout.setOnClickListener(this);
	}

	// 监听不同按钮执行不同操作
	@Override
	public void onClick(View v) {
		int id = v.getId();
		// 点击则进入修改用户信息界面
		if (id == R.id.changeuserinfo) {
			Utils.goActivity(ctx, PersonalInfoSettingActivity.class);
		} else if (id == R.id.courseinfo) {//点击则进入我的课程表界面
			Utils.goActivity(ctx, MyCourseActivity.class);	
		} else if (id == R.id.newsinfo) {//点击则进入最新资讯界面
			Utils.goActivity(ctx, InformationActivity.class);
		} else if (id == R.id.settinginfo) {//点击则进入设置界面
			Utils.goActivity(ctx, SettingActivity.class);
		}
	}

	
}
