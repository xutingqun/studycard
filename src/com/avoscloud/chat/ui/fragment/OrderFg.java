package com.avoscloud.chat.ui.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.example.studycard.R;
import com.studycard.adapter.MyExpandanleListViewAdapter;
import com.studycard.bean.CategoryBean;
import com.studycard.ui.OrderFgChildActivity;
import com.studycard.ui.OrderFgChildActivity.DataLoadListener;
import com.studycard.ui.SubjectManageActivity;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class OrderFg extends BaseFragment {
	private ExpandableListView indexLV;
	private ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();;
	private String name;
	private ArrayList<HashMap<String, Object>> categoryList = new ArrayList<HashMap<String, Object>>();
	private HashMap<Object, ArrayList<CategoryBean>> child_List = new HashMap<Object, ArrayList<CategoryBean>>();
	private MyExpandanleListViewAdapter ExpandListAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.training, container, false);
	}

	@SuppressLint("NewApi")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		 headerLayout.showTitle(R.string.bookcourse);
	    getData();
			indexLV = (ExpandableListView) getView().findViewById(R.id.lv);

			indexLV.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView arg0, View arg1,
						final int groupPosition, long arg3) {
					// TODO Auto-generated method stub

					String uuid = (String) list.get(groupPosition).get("uuid");
					OrderFgChildActivity category = new OrderFgChildActivity(
							uuid);
					category.TrainCategory(new DataLoadListener() {
						@Override
						public void onDataLoadSuccess(
								ArrayList<HashMap<String, Object>> list) {
							categoryList.clear();
							categoryList.addAll(list);
							Log.e("handleMessage", "categoryList:" + child_List);
							ExpandListAdapter.notifyDataSetChanged();
						}

					});

					return false;
				}
			});
			indexLV.setOnChildClickListener(new OnChildClickListener() {

				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					// TODO Auto-generated method stub
					Intent intent = new Intent();
					intent.putExtra("childPosition", childPosition);
					intent.putExtra("categoryList", categoryList);
					intent.setClass(ctx, SubjectManageActivity.class);
					startActivity(intent);
					return false;
				}

			});

	}

	public void getData() {
		AVQuery<AVObject> avQuery = new AVQuery<AVObject>("category");
		avQuery.findInBackground(new FindCallback<AVObject>() {

			@Override
			public void done(List<AVObject> arg0, AVException arg1) {
				for (int i = 0; i < arg0.size(); i++) {
					AVObject avObject = arg0.get(i);
					HashMap<String, Object> hashMap = new HashMap<String, Object>();
					hashMap.put("categoryName", avObject.getString("name"));
					hashMap.put("uuid", avObject.getObjectId());
					list.add(hashMap);

				}
				ExpandListAdapter = new MyExpandanleListViewAdapter(
						ctx, list, categoryList);
				indexLV.setAdapter(ExpandListAdapter);
			}

		});
	}
}
